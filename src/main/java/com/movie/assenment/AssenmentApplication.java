package com.movie.assenment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssenmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssenmentApplication.class, args);
	}
}
